package com.positive_apps.com.checks;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.util.Arrays;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends Activity {


    private WebView mWebView;
    private FrameLayout troubleshoot;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        requestWindowFeature(Window.FEATURE_PROGRESS);

        setContentView(R.layout.activity_main);

        setProgressBarIndeterminateVisibility(true);
        setProgressBarVisibility(true);



        try {
            mWebView = (WebView) findViewById(R.id.webview);
            troubleshoot = (FrameLayout) findViewById(R.id.troubleshoot);
            troubleshoot.setVisibility(View.GONE);

            mWebView.getSettings().setJavaScriptEnabled(true);
            mWebView.setWebViewClient(new WebViewClient() {
                public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                    troubleshoot.setVisibility(View.VISIBLE);
                    Toast.makeText(MainActivity.this, getString(R.string.problem_in_connection), Toast.LENGTH_SHORT).show();
                }
            });


            mWebView.setWebChromeClient(new WebChromeClient() {
                public void onProgressChanged(WebView view, int progress) {
                    setProgress(progress * 100);
                    if (progress == 100) {
                        setProgressBarIndeterminateVisibility(false);
                        setProgressBarVisibility(false);
                    }
                }
            });

            mWebView.loadUrl("http://checks.ml");
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Browser: " + e.getMessage());
            troubleshoot.setVisibility(View.VISIBLE);
            Toast.makeText(MainActivity.this, getString(R.string.problem_in_connection), Toast.LENGTH_SHORT).show();
        }



        Call<Version> call = MyApp.apiService.getVersion();

        call.enqueue(new Callback<Version>() {
            @Override
            public void onResponse(Call<Version> call, Response<Version> response) {
                if(response != null) {
                    checkVersionForUpdate(response.body().android);
                }
            }

            @Override
            public void onFailure(Call<Version> call, Throwable t) {
                Log.i("Rat", "onFailure: ");
            }
        });


    }

    public void onTryAgainClick(View v){
        finish();
        startActivity(getIntent());
    }

    public void checkVersionForUpdate(String newServerVersion) {

        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        String version = pInfo.versionName;

        // split the version's string
        String[] oldVersion = version.split("\\.");
        String[] newVersion = newServerVersion.split("\\.");

        // check array size for safety

        if (newVersion.length == 0) {
            newVersion = new String[]{"0", "0", "0"};
        }
        if (newVersion.length == 1) {
            newVersion = new String[]{newVersion[0], "0", "0"};
        }
        if (newVersion.length == 2) {
            newVersion = new String[]{newVersion[0], newVersion[1], "0"};
        }
        if (oldVersion.length == 1) {
            oldVersion = new String[]{"0", "0", "0"};
        }
        if (oldVersion.length == 2) {
            oldVersion = new String[]{oldVersion[0], "0", "0"};
        }
        if (oldVersion.length == 3) {
            oldVersion = new String[]{oldVersion[0], oldVersion[1], "0"};
        }

        Log.i("Versions", "oldVersion: " + Arrays.toString(oldVersion));
        Log.i("Versions", "newVersion: " + Arrays.toString(newVersion));

        // check for critic changes
        if (Integer.parseInt(newVersion[0]) > Integer.parseInt(oldVersion[0])) {
            Log.i("Versions", "first " + Integer.parseInt(newVersion[0]) + " | " + Integer.parseInt(oldVersion[0]));
            forceUpdateDialog();
        } else if (Integer.parseInt(newVersion[1]) > Integer.parseInt(oldVersion[1])) {
            if (Integer.parseInt(newVersion[0]) == Integer.parseInt(oldVersion[0])) {
                Log.i("Versions", "second " + Integer.parseInt(newVersion[1]) + " | " + Integer.parseInt(oldVersion[1]));
                informUpdateDialog();
            }
        } else if (Integer.parseInt(newVersion[2]) > Integer.parseInt(oldVersion[2])) {
            if (Integer.parseInt(newVersion[0]) == Integer.parseInt(oldVersion[0])
                    && Integer.parseInt(newVersion[1]) == Integer.parseInt(oldVersion[1])) {
                Log.i("Versions", "third " + Integer.parseInt(newVersion[2]) + " | " + Integer.parseInt(oldVersion[2]));
                informUpdateDialog();
            }
        }
    }

    private void forceUpdateDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle);
        builder.setCancelable(false);
        builder.setTitle(getString(R.string.update_version_title));
        builder.setMessage(getString(R.string.new_update_available));
        builder.setPositiveButton(getString(R.string.update_version), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                goToStoreForUpdate();
            }
        });
        builder.setNegativeButton(getString(R.string.exit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.show();
    }

    private void informUpdateDialog() {
        // force dialogg
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle);
        builder.setTitle(getString(R.string.update_version_title));
        builder.setMessage(getString(R.string.new_update_available));
        builder.setPositiveButton(getString(R.string.update_version), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                goToStoreForUpdate();
            }
        });
        builder.setNegativeButton(getString(R.string.cancel), null);
        builder.show();
    }

    public void goToStoreForUpdate() {
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }




}
