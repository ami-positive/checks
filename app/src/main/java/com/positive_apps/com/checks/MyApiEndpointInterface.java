package com.positive_apps.com.checks;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Izakos on 06/04/2016.
 */
public interface MyApiEndpointInterface {

    @GET("/api/version")
    Call<Version> getVersion();
}
