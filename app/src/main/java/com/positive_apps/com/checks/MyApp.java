package com.positive_apps.com.checks;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by natiapplications on 16/08/15.
 */
public class MyApp extends Application implements Application.ActivityLifecycleCallbacks {


    public static final String TAG = "Application";

    public static Context appContext;
    public static String appVersion;
    public static MyApiEndpointInterface apiService;


    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();

        registerActivityLifecycleCallbacks(this);
        appContext = getApplicationContext();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://checks.ml/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        // prepare call in Retrofit 2.0
        System.out.println("======== " + retrofit.create(MyApiEndpointInterface.class));
        apiService = retrofit.create(MyApiEndpointInterface.class);


    }


    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {
        Log.i(TAG, "onActivityResumed: " + activity);

    }

    @Override
    public void onActivityPaused(Activity activity) {
    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }


}
